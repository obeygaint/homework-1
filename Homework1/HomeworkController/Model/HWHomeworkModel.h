//
//  HWHomeworkModel.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HWHomeworkChildController.h"

@interface HWHomeworkModel : NSObject

@property (nonatomic, strong,readonly) NSArray<Class<HWHomeworkChildController>> *controllerClasses;

- (void)pointForTime:(NSTimeInterval)time
               size:(CGSize)size
      withCompletion:(void(^)(CGPoint point))completion;

- (void)convertString:(NSString *)string
       withCompletion:(void(^)(NSString *convertedString))completion;

- (void)createMatrixWithRowsCount:(NSInteger)rowsCount
                     columnsCount:(NSInteger)columnsCount
                       completion:(void(^)(NSArray<NSArray<NSNumber *> *> *matrix))completion;



@end
