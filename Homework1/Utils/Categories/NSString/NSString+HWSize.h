//
//  NSString+HWSize.h
//  HelpWill
//
//  Created by Viktor Peschenkov on 05.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

@interface NSString (HWSize)

- (CGSize)hw_sizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size
            lineBreakMode:(NSLineBreakMode)lineBreakMode;

- (CGSize)hw_sizeWithAttributes:(NSDictionary *)attributes
              constrainedToSize:(CGSize)size;

- (CGSize)hw_sizeWithAttributes:(NSDictionary *)attributes
                       forWidth:(CGFloat)width;

- (CGSize)hw_sizeWithFont:(UIFont *)font
                 forWidth:(CGFloat)width
            lineBreakMode:(NSLineBreakMode)lineBreakMode;

- (CGSize)hw_sizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size;

- (CGSize)hw_sizeWithFont:(UIFont *)font;

@end
