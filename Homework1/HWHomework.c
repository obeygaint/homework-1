//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
//#include "HWAnimationConstants.h"
#include "stdlib.h"
#include <math.h>
#include <string.h>
#define CHECK(x,y,l,r) ( (x > l && x < r && y > l && y < r) ? 1 : 0)

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
int const MAX=100;// границы для генератора случайных чисел.
int const MIN=-100;


long** createMatrix(const long rows, const long columns) {
    long **matrix = (long**) malloc(rows*sizeof(long));
    for(long i=0;i<rows;++i)
    {
        matrix[i]=(long*) malloc(columns*sizeof(long));
        for(long j=0;j<columns;++j)
            matrix[i][j]=rand()%(MAX-MIN+1)+MIN;
    }
    return matrix;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
int const ASCII_CONVERT_CONST=48;
char* const SPACE=" ";

unsigned long countMemory(const char *str)// Функция для подсчета кол-ва необходимой памяти.
{
    int memcount[10]={5,5,4,4,7,5,6,5,7,4};
    unsigned size=0;
    for(unsigned long i=0;i<strlen(str);++i){
        if(str[i]=='.')
        {
            size+=6;
            continue;
        }
        size+=memcount[str[i]-ASCII_CONVERT_CONST];
    }
    return size;
}
char* stringForNumberString(const char *numberString) {
    char *numbers[10]={"Ноль","Один","Два","Три","Четыри","Пять","Шесть","Семь","Восемь","Девять"};
    char *point="Точка";
    char *result=(char*)malloc(sizeof(char)*countMemory(numberString)*2-1);
    for(int i=0;i<strlen(numberString);++i)
    {
        if(numberString[i]=='.')
        {
            result=strcat(result, point);
            if(i!=strlen(numberString)-1)
                result=strcat(result, SPACE);
            continue;
        }
        result=strcat(result, numbers[numberString[i]-ASCII_CONVERT_CONST]);
        if(i!=strlen(numberString)-1)
            result=strcat(result, SPACE);
    }
    return result;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
//double spinTime=0;
double curTime=0;
double iterRemebed=0;
int offset=0;
_Bool reverse=0;
int iterations=0;
int curiter=0;
double Ttime=0;
double data[100];
HWPoint pointForAnimationTime(double time, const double canvasSize) {
    offset=canvasSize/2.0;
    time=time*5;
    //time=lround(time);
    int param=25;
    double x=time*param*sin(time)+offset;
    double y=time*param*cos(time)+offset;
    if(!iterRemebed){
        data[iterations]=time;
        ++iterations;
        if(x+8.0f>=canvasSize || y+8.0f>=canvasSize)
        {
            curTime=time;
            iterRemebed=iterations;
            reverse=1;
        }
        //printf("X:%f Y:%f /n",x,y);
        return (HWPoint){x,y};
    }
    if(reverse)
    {
        if(iterations==0)
        {
            reverse=0;
        }
        else{
            iterations--;
        }
        Ttime=data[iterations];
    }
    else{
        if(iterations==iterRemebed-1)
            reverse=1;
        else{
            iterations++;
        }
        Ttime=data[iterations];
    }
    x=Ttime*param*sin(Ttime)+offset;
    y=Ttime*param*cos(Ttime)+offset;
    return (HWPoint){x,y};
}

