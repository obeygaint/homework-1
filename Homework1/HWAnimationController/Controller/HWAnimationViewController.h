//
//  HWAnimationViewController.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 28/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWHomeworkChildController.h"

@protocol HWAnimationViewControllerDelegate;

@interface HWAnimationViewController : UIViewController <HWHomeworkChildController>

@property (nonatomic, weak) id<HWAnimationViewControllerDelegate> delegate;

- (void)addPoint:(CGPoint)point;

@end

@protocol HWAnimationViewControllerDelegate <NSObject>

@required
- (void)animationController:(HWAnimationViewController *)controller needPointForAnimationTime:(NSTimeInterval)time;

@end
